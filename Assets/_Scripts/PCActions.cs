﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Phijam
{
	public abstract class PCAction
	{
		protected PCMotor pcm { get { return PCController.Instance.motor; } }
		protected PCInventory pci { get { return PCController.Instance.inventory; } }
		//protected PCEmotes pce { get { return }}
		protected bool runUpdate;
		
		public abstract void StartAction();
		public abstract void ActionUpdate();
		public abstract bool IsComplete();
		public abstract void FinishAction();
	}
	
	public interface IRoomTransferAction
	{
		RoomNode GetFinalRoom();
	}
	
	public class SimpleMove : PCAction
	{
		float targetX;
		public bool isInterruptable;
		
		public SimpleMove(float x, bool interruptable = true)
		{
			targetX = x;
			isInterruptable = interruptable;
		}
		
		public override void StartAction() { runUpdate = true; }
		
		public override void ActionUpdate()
		{
			if (!runUpdate)
				return;
			pcm.xDir = Mathf.Sign(targetX - pcm.transform.position.x);
		}
		
		public override bool IsComplete()
		{
			return Mathf.Abs(targetX - pcm.transform.position.x) < .1f;
		}
		
		public override void FinishAction()
		{
			pcm.xDir = 0f;
		}
	}
	
	public class GoThroughPortal : PCAction, IRoomTransferAction 
	{
		RoomPortal portal;
		
		float portalTransitionTime = 1f;
		float timer = 0f;

        bool leftToRight;
		
		public GoThroughPortal(RoomPortal rp) { portal = rp; }
		
		public override void StartAction()
		{
			runUpdate = true;
            pcm.xDir = Mathf.Sign(portal.connectedPortal.transform.position.x - portal.transform.position.x);
            leftToRight = pcm.xDir > 0;
		}
		
		public override void ActionUpdate()
		{
			if (!runUpdate)
				return;
			timer += Time.deltaTime;
		}
		
		public override bool IsComplete()
		{
            return (timer >= portalTransitionTime
                || leftToRight && pcm.transform.position.x > portal.connectedPortal.transform.position.x
                || !leftToRight && pcm.transform.position.x < portal.connectedPortal.transform.position.x);
		}
		
		public override void FinishAction()
		{
			PCController.Instance.currentRoom = portal.connectedPortal.ownerRoom;
		}
		
		public RoomNode GetFinalRoom() { return portal.connectedPortal.ownerRoom; }
	}
	
	public class GoThroughBackwallPortal : PCAction, IRoomTransferAction
	{
		RoomPortal portal;
		
		//0 going in 1 hidden 2 going out 3 finished
		int transitionState = -1;
		float portalTransitionTime = .5f;
		float hiddenTime = 1.4f;
		
		float depthTweenTime = 2f;
		
		float timer = 0f;
		
		public GoThroughBackwallPortal(RoomPortal rp) { portal = rp; }

        Vector3 initialBgPos;
        Vector3 finalBgPos;

		public override void StartAction()
		{
            initialBgPos = portal.transform.position + Vector3.forward * .35f;
			initialBgPos.y = portal.ownerRoom.transform.position.y;

            finalBgPos = portal.connectedPortal.transform.position + Vector3.forward * .35f;
            finalBgPos.y = portal.connectedPortal.ownerRoom.transform.position.y;

            pcm.zDir = 1f;
			pcm.transform.DOMove(initialBgPos, depthTweenTime).SetEase(Ease.Linear).
				OnComplete(() => {
					pcm.zDir = 0f;
					transitionState = 0;
					runUpdate = true;
				});
		}
		
		public override void ActionUpdate()
		{
			if (!runUpdate)
				return;
			timer += Time.deltaTime;
			switch (transitionState)
			{
			case 0:
				if (timer > portalTransitionTime)
				{
					transitionState++;
					timer = 0f;
					pcm.anim.gameObject.SetActive(false);
				}
				break;
			case 1:
				if (timer > hiddenTime)
				{
					transitionState++;
					timer = 0f;
					
					pcm.transform.position = finalBgPos;
					
					pcm.anim.gameObject.SetActive(true);
					
					pcm.zDir = -1f;
					
					var tweenPos = finalBgPos;
					tweenPos.z = 0f;
					pcm.transform.DOMove(tweenPos, depthTweenTime).SetEase(Ease.Linear)
						.OnComplete(() => transitionState++);
				}
                else
                    {
                        pcm.transform.position = Vector3.Lerp(initialBgPos, finalBgPos, timer / hiddenTime);
                    }
				break;
			default:
				break;
			}
		}
		
		public override bool IsComplete()
		{
			return transitionState == 3;
		}
		
		public override void FinishAction()
		{
			pcm.zDir = 0f;
			PCController.Instance.currentRoom = portal.connectedPortal.ownerRoom;
		}
		
		public RoomNode GetFinalRoom() { return portal.connectedPortal.ownerRoom; }
	}
	
	public class PickOrSwapItem : PCAction
	{
		InventoryItem item;
		float actionTime = 1f;
		float timer = 0f;
		
		public PickOrSwapItem(InventoryItem i) { item = i; }
		
		public override void StartAction()
		{
			pcm.zDir = .1f;
			runUpdate = true;
		}
		
		public override void ActionUpdate()
		{
			if (!runUpdate)
				return;
			timer += Time.deltaTime;
		}
		
		public override bool IsComplete()
		{
			return (timer >= actionTime);
		}
		
		public override void FinishAction()
		{
			pcm.zDir = 0f;
			
			pci.LeaveItem(item.transform);
			pci.HoldItem(item);
		}
	}
	
	public class ObstaclePanic : PCAction
	{
		float frontLookDuration = 1f;
		float timer = 0f;
		Obstacle obstacle;
		System.Action onFinished;
		
		public ObstaclePanic(Obstacle o, System.Action onFinished) { obstacle = o; this.onFinished = onFinished; }
		
		public override void StartAction()
		{
			pcm.zDir = -.1f;
			runUpdate = true;
            SingletonUtils<PCEMotes>.Instance.ShowEmote(EEmotes.Neg, frontLookDuration);
		}
		
		public override void ActionUpdate()
		{
			if (!runUpdate)
				return;
			timer += Time.deltaTime;
		}
		
		public override bool IsComplete()
		{
			return (timer >= frontLookDuration);
		}
		
		public override void FinishAction()
		{
			pcm.zDir = 0f;
			if (null != onFinished)
				onFinished();
		}
	}

    public class InteractWith : PCAction
    {
        InteractionTarget interactive;

        //TODO: probably particles
        float interactionDuration = 1f;
        float timer = 0f;

        public InteractWith(InteractionTarget it)
        {
            interactive = it;
        }

        public override void StartAction()
        {
            runUpdate = true;
            pcm.zDir = .1f;
        }

        public override void ActionUpdate()
        {
            if (!runUpdate)
                return;
            timer += Time.deltaTime;
        }

        public override bool IsComplete()
        {
            return (timer >= interactionDuration);
        }

        public override void FinishAction()
        {
            Debug.Log("finish");
            pcm.zDir = 0f;

            if (null != interactive.onCompleteCallback)
                interactive.onCompleteCallback.Invoke();
        }
    }
}