﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class CameraEffects : MonoBehaviour
	{
		[SerializeField] Material material;

		public void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			// Copy the source Render Texture to the destination,
			// applying the material along the way.
			Graphics.Blit(source, destination, material);
		}
	}
}
