﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class InputManager : MonoBehaviour
	{
		public LayerMask clickableLayers;
		
		Camera rayCam;
		
		int lWalkable;
		int lItems;
        int lInteractives;
        int lPlayer;
		
		public Action<RoomNode, Vector3, bool> WalkToEvent;
		public Action<InventoryItem> PickItemEvent;
        public Action<InteractionTarget> InteractionEvent;
        public Action SelfClickEvent;
		
		RaycastHit[] hitCache = new RaycastHit[16];

        PCController pc;
		
		void Awake()
		{
            pc = PCController.Instance;

			if (!rayCam)
				rayCam = Camera.main;
				
			lWalkable = LayerMask.NameToLayer("WalkableZone");
			lItems = LayerMask.NameToLayer("InventoryItems");
            lInteractives = LayerMask.NameToLayer("Interactives");
            lPlayer = LayerMask.NameToLayer("Player");
		}
		
		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
				Application.Quit();
			if (PCController.Instance.ShouldIgnoreInput())
				return;
			if (Input.GetMouseButtonUp(0))
			{
				var ray = rayCam.ScreenPointToRay(Input.mousePosition);
				var rHits = Physics.RaycastNonAlloc(ray, hitCache, Mathf.Infinity, clickableLayers);
				if (rHits > 0)
				{
					for (int i = 0; i < rHits; ++i)
					{
                        var hit = hitCache[i];
						if (hit.collider.gameObject.layer == lItems)
						{
							var item = hit.collider.GetComponent<InventoryItem>();
							if (item)
							{
								PickItemEvent(item);
								return;
							}
						}
                        else if (hit.collider.gameObject.layer == lInteractives)
                        {
                            var interactive = hit.collider.GetComponent<InteractionTarget>();
                            Debug.Log(interactive.name);
                            if (interactive)
                            {
								if ((!interactive.requireItem ||
								     null != pc.inventory.heldItem && interactive.requiredIKey == pc.inventory.heldItem.iKey)
								    && ((1 << pc.PerceptionLayer) & (int)interactive.requirePerception) != 0)
								{
									InteractionEvent(interactive);
								}
                                else
                                {
                                    Debug.LogWarning("no required item");
                                    WalkToEvent(interactive.GetHostRoom(), interactive.transform.position, false);
                                }
                                return;
                            }
                        }
                        else if (hit.collider.gameObject.layer == lPlayer)
                        {
                            if (null != SelfClickEvent)
                                SelfClickEvent();
                            return;
                        }
					}
					
					for (int i = 0; i < rHits; ++i)
					{
						var hit = hitCache[i];
						var room = hit.collider.GetComponentInParent<RoomNode>();
						if (room)
						{
							WalkToEvent(room, hit.point, false);
							return;
						}
					}
				}
			}
		}
	}
}