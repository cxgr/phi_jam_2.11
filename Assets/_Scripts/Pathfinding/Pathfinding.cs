﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Phijam
{
	public class Pathfinding : MonoBehaviour
	{
		[SerializeField] RoomNode initialNode;
		Dictionary<RoomNode, List<RoomNode>> graph = new Dictionary<RoomNode, List<RoomNode>>();
		
		void Awake()
		{
			RebuildGraph();
		}
		
		public void RebuildGraph()
		{
			graph.Clear();
			var allRooms = FindObjectsOfType<RoomNode>();
			foreach (var room in allRooms)
			{
				room.portals = room.GetComponentsInChildren<RoomPortal>();
				foreach (var portal in room.portals)
				{
					portal.ownerRoom = room;
					if (portal.connectedPortal)
						portal.connectedPortal.connectedPortal = portal; //yo dawg
				}
			}
			
			foreach (var room in allRooms)
			{
				if (!graph.ContainsKey(room))
					graph.Add(room, new List<RoomNode>());
				foreach (var p in room.portals)
					if (p.connectedPortal && !graph[room].Contains(p.connectedPortal.ownerRoom))
						graph[room].Add(p.connectedPortal.ownerRoom);
			}
		}
		
		public List<RoomNode> GetPath(RoomNode fromRoom, RoomNode toRoom)
		{
			var q = new Queue<RoomNode>();
			var visited = new Dictionary<RoomNode, RoomNode>();
			
			q.Enqueue(fromRoom);
			
			while (q.Count > 0)
			{
				var room = q.Dequeue();
				foreach (var adj in graph[room])
					if (!visited.ContainsKey(adj))
					{
						visited.Add(adj, room);
						q.Enqueue(adj);
					}
			}
			
			if (!visited.ContainsKey(toRoom))
				return new List<RoomNode>();
			
			var path = new List<RoomNode>();
			var curRoom = toRoom;
			while (curRoom.GetInstanceID() != fromRoom.GetInstanceID())
			{
				path.Add(curRoom);
				curRoom = visited[curRoom];
			};
			path.Reverse();
			return path;
		}
		
		public void DisconnectRooms(RoomNode rn1, RoomNode rn2)
		{
			if (graph.ContainsKey(rn1))
				graph[rn1].Remove(rn2);
			if (graph.ContainsKey(rn2))
				graph[rn2].Remove(rn1);
		}
		
		public void ConnectRooms(RoomNode rn1, RoomNode rn2)
		{
			if (graph.ContainsKey(rn1) && rn1.portals.Any(p => p.connectedPortal.ownerRoom == rn2))
				if (!graph[rn1].Contains(rn2))
					graph[rn1].Add(rn2);
			if (graph.ContainsKey(rn2) && rn2.portals.Any(p => p.connectedPortal.ownerRoom == rn1))
				if (!graph[rn2].Contains(rn1))
					graph[rn2].Add(rn1);
		}
		
		void OnDrawGizmos()
		{
			Gizmos.color = Color.green;
			var offset = Vector3.up * 1.5f;
			foreach (var kvp in graph)
				foreach (var v in kvp.Value)
					Gizmos.DrawLine(kvp.Key.transform.position + offset, v.transform.position + offset);
		}
		
		public RoomNode rn1;
		public RoomNode rn2;
		public bool testDisconnect;
		public bool testConnect;
		void Update()
		{
			if (testDisconnect)
			{
				testDisconnect = false;
				DisconnectRooms(rn1, rn2);
			}
			if (testConnect)
			{
				testConnect = false;
				ConnectRooms(rn1, rn2);
			}
		}
	}
	
	#if UNITY_EDITOR
	[CustomEditor(typeof(Pathfinding))]
	public class Pathfinding_Editor : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
	
			Pathfinding pathfinding = target as Pathfinding;
			if (GUILayout.Button("rebuild graph"))
			{
				pathfinding.RebuildGraph();
				foreach (var r in FindObjectsOfType<RoomNode>())
					EditorUtility.SetDirty(r.gameObject);
				foreach (var rp in FindObjectsOfType<RoomPortal>())
					EditorUtility.SetDirty(rp.gameObject);
				EditorUtility.SetDirty(pathfinding.gameObject);
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(
					UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
			}
		}
	}
	#endif
}