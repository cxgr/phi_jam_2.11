﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class InventoryItemModifierHack : MonoBehaviour
	{
		[SerializeField] InventoryItem.ItemKeys keyModifier;
		[SerializeField] InventoryItem targetItem;

		public void Apply()
		{
			targetItem.iKey = keyModifier;
		}
	}
}
