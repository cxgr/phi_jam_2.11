﻿using UnityEngine;

namespace Phijam
{
	public class CameraBlend : MonoBehaviour
	{
		[SerializeField] Camera realCamera;
		[SerializeField] Camera imaginaryCamera;
		[SerializeField] [Range(0, 1)] float blendWeight;

		RenderTexture realRT;
		RenderTexture imaginaryRT;

		[SerializeField] Material baseMaterial;
		[SerializeField] Material overlayMaterial;
		int _weightPropertyId;
		[SerializeField] [Range(1, 4)] int qualityMultiplier = 1;

		private void Awake()
		{
			_weightPropertyId = Shader.PropertyToID("_Weight");
			BlendWeight = blendWeight;

			realRT = RenderTexture.GetTemporary(Screen.width * qualityMultiplier, Screen.height * qualityMultiplier);
			realCamera.targetTexture = realRT;

			imaginaryRT = RenderTexture.GetTemporary(Screen.width * qualityMultiplier, Screen.height * qualityMultiplier);
			imaginaryCamera.targetTexture = imaginaryRT;
		}

		public float BlendWeight
		{
			get { return blendWeight; }
			set
			{
				blendWeight = value;
				overlayMaterial.SetFloat(_weightPropertyId, blendWeight);
			}
		}

		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			Graphics.Blit(realRT, destination, baseMaterial);
			Graphics.Blit(imaginaryRT, destination, overlayMaterial);
		}

#if UNITY_EDITOR
		private void LateUpdate()
		{
			BlendWeight = blendWeight;
		}
#endif
	}
}
