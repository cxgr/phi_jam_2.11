﻿using UnityEngine;
using System.Linq;
using UnityEngine.Events;

namespace Phijam
{
    [System.Serializable]
    public class InteractionEvent : UnityEvent
    {

    }

	public class InteractionTarget : MonoBehaviour
	{
        [Header("should be parented to a room node")]
        [Header("-------------------")]
        [Header("if required item isn't carried, this is just a goto point")]
        public bool requireItem;
        [Header("enum with keys is in InventoryItem class")]
        public InventoryItem.ItemKeys requiredIKey;
		public PCController.PerceptionLayers requirePerception = PCController.PerceptionLayers.Any;

        [Header("action to perform on successful interaction")]
        [Header("there's no condition checking mid-interaction")]
        [Header("interaction shouldn't begin if some conditions aren't met")]
        public InteractionEvent onCompleteCallback;

        void Awake()
        {
            if (null == GetHostRoom())
            {
                Debug.LogError(name + " item wasnt parented properly at startup");
                transform.SetParent(FindObjectsOfType<RoomNode>()
                    .OrderBy(r => Vector3.Distance(transform.position, r.transform.position))
                    .FirstOrDefault().transform, true);
            }
            Debug.Log(GetHostRoom());
        }

        public RoomNode GetHostRoom()
        {
            return GetComponentInParent<RoomNode>();
        }

		public void Show(SpriteRenderer sprite)
		{
			DG.Tweening.DOTweenModuleSprite.DOFade(sprite, 1f, 1f);
		}

		public void Hide(SpriteRenderer sprite)
		{
			DG.Tweening.DOTweenModuleSprite.DOFade(sprite, 0f, 1f);
		}

		public void HideGroup(Transform group)
		{
			var renderers = group.GetComponentsInChildren<SpriteRenderer>();
			for (int i = 0; i < renderers.Length; ++i)
			{
				Hide(renderers[i]);
			}
		}
	}
}
