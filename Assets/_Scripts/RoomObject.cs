﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Phijam
{
	public class RoomObject : MonoBehaviour
	{
		public enum RoomKeys
		{
			Storage,
			Kitchen,
			Living,
			Hall,
			Bedroom,
			Study,
			Library,
			Bathroom,
			Attic,
			Basement,
			Outside,
		}
		
		[SerializeField] GameObject baseCube;
		[SerializeField] float charactersDepthOffset = -1f;
		[SerializeField] float wid = 12f;
		[SerializeField] float hei = 3f;
		[SerializeField] float dep = 4f;
		[SerializeField] float wallThickness = .2f;
		[SerializeField] float floorThickness = .5f;
		
		[SerializeField] BoxCollider clickableZone;
		
		public RoomKeys roomKey;
		
		RoomNode node;
		public RoomNode Node
		{
			get
			{
				if (null == node)
					node = GetComponent<RoomNode>();
				return node;
			}
		}
		
		public void GenerateRoom()
		{
			if (!Application.isEditor || Application.isPlaying)
				return;
				
			var oldCubes = GetComponentsInChildren<Transform>().Where(o => o.name.Contains("room_creator_block"));
			foreach (var oc in oldCubes)
				DestroyImmediate(oc.gameObject);
			
			transform.localScale = Vector3.one;
			
			var floor = SpawnCube();
			floor.transform.localScale = new Vector3(wid, floorThickness, dep);
			floor.transform.localPosition = new Vector3(0f, - floorThickness / 2f, dep / 2f + charactersDepthOffset);
			
			var wallLeft = SpawnCube();
			wallLeft.transform.localScale = new Vector3(wallThickness, hei + floorThickness * 2, dep);
			wallLeft.transform.localPosition = new Vector3(-(wid + wallThickness) / 2f, hei / 2f, dep / 2f + charactersDepthOffset);
			
			var wallRight = SpawnCube();
			wallRight.transform.localScale = new Vector3(wallThickness, hei + floorThickness * 2, dep);
			wallRight.transform.localPosition = new Vector3((wid + wallThickness) / 2f, hei / 2f, dep / 2f + charactersDepthOffset);
			
			var ceiling = SpawnCube();
			ceiling.transform.localScale = new Vector3(wid, floorThickness, dep);
			ceiling.transform.localPosition = new Vector3(0f, hei + floorThickness / 2f, dep / 2f + charactersDepthOffset);
			
			var backWall = SpawnCube();
			backWall.transform.localScale = new Vector3(wid, hei, wallThickness);
			backWall.transform.localPosition = new Vector3(0f, hei / 2f, dep + wallThickness / 2f + charactersDepthOffset);
			
			var cZone = transform.Find("clickable_zone");
			if (!cZone)
			{
				cZone = new GameObject("clickable_zone").transform;
				cZone.parent = transform;
				cZone.localPosition = Vector3.zero;
				cZone.gameObject.AddComponent<BoxCollider>();
			}
			float cZoneThk = .1f;
			clickableZone = cZone.GetComponent<BoxCollider>();
			clickableZone.size = new Vector3(wid, hei / 2f, cZoneThk);
			clickableZone.center = new Vector3(0f, clickableZone.size.y / 2f - floorThickness, -charactersDepthOffset + cZoneThk / 2f);
			clickableZone.gameObject.layer = LayerMask.NameToLayer("WalkableZone");
		}
		
		public void ResetPortals()
		{
			if (!Application.isEditor || Application.isPlaying)
				return;
				
			var portals = GetComponentsInChildren<RoomPortal>();
			foreach (var p in portals)
			{
				var node = GetComponent<RoomNode>();
				p.ownerRoom = node;
				p.connectedPortal = null; 
			}
		}
		
		public void ConnectNearest()
		{
			
			#if UNITY_EDITOR
			if (!Application.isEditor || Application.isPlaying)
				return;
				
			var node = GetComponent<RoomNode>();
			if (node)
			{
				var allPortals = FindObjectsOfType<RoomPortal>().
					Where(rp => rp.ownerRoom != node);
				var childPortals = GetComponentsInChildren<RoomPortal>(); 
				foreach (var p in childPortals)
				{
					p.ownerRoom = node;
					EditorUtility.SetDirty(p.gameObject);
				}
				foreach (var p in childPortals)
				{
					p.connectedPortal = allPortals
					.OrderBy(otherPortal => Vector3.Distance(p.transform.position, otherPortal.transform.position))
						.FirstOrDefault();
					EditorUtility.SetDirty(p.gameObject);
				}
			}
			#endif
		}
		
		GameObject SpawnCube() { return Instantiate(baseCube, transform.position, Quaternion.identity, transform); }
	}
	
	#if UNITY_EDITOR
	[CustomEditor(typeof(RoomObject))]
	public class RoomObject_Editor : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
	
			RoomObject ro = target as RoomObject;
            GUILayout.Label("ROOM GENERATOR IS OBSOLETE", new GUIStyle("CN EntryError"));
            GUILayout.Label("DO NOT USE WITH PACKED ROOMS", new GUIStyle("CN EntryError"));
            if (GUILayout.Button("generate room"))
			{
				ro.GenerateRoom();
				EditorUtility.SetDirty(ro.gameObject);
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(
					UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
			}
			
			if (GUILayout.Button("reset portals"))
			{
				ro.ResetPortals();
				EditorUtility.SetDirty(ro.gameObject);
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(
					UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
			}
			
			if (GUILayout.Button("connect nearest"))
			{
				ro.ConnectNearest();
				EditorUtility.SetDirty(ro.gameObject);
				foreach (var p in ro.GetComponent<RoomNode>().portals)
				{
					EditorUtility.SetDirty(p.gameObject);
					if (PrefabUtility.GetPrefabType(p) == PrefabType.PrefabInstance)
						PrefabUtility.DisconnectPrefabInstance(p);
				}
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(
					UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
			}
		}
	}
	#endif
}