﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Phijam
{
	public class InventoryItem : MonoBehaviour
	{
		public enum ItemKeys
		{
			Nothing,
			EmptyGlass,
			GlassWithWater,
			Picture,
			Fire,
			Paints,
			Button,
			Mask,
		}

        [Header("should be parented to a room node")]
        [Header("-------------------")]
        public ItemKeys iKey;
		
		void Awake()
		{			
			if (null == GetHostRoom())
			{
				Debug.LogError(name + " item wasnt parented properly at startup");
				transform.SetParent(FindObjectsOfType<RoomNode>()
					.OrderBy(r => Vector3.Distance(transform.position, r.transform.position))
					.FirstOrDefault().transform, true);
			}
		}
		
		public RoomNode GetHostRoom()
		{
			return GetComponentInParent<RoomNode>();
		}
		
		public void ToggleClickability(bool on) { GetComponentInChildren<Collider>().enabled = on; }
		
		void OnDrawGizmos()
		{
			Gizmos.color = Color.white;
			var room = GetHostRoom();
			if (room)
				Gizmos.DrawLine(transform.position, GetHostRoom().transform.position);
		}
	}
}