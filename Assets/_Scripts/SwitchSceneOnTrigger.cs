﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Phijam
{
	public class SwitchSceneOnTrigger : MonoBehaviour
	{
		[SerializeField] Transform player;

		void LateUpdate()
		{
			if ((player.position - transform.position).sqrMagnitude < 1f)
			{
				int idx = SceneManager.GetActiveScene().buildIndex;
				SceneManager.LoadScene(idx + 1);
			}
		}
	}
}