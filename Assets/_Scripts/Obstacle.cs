﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Phijam
{
	public class Obstacle : MonoBehaviour
	{
        [Header("on collision PC checks for non null in this order:")]
		[Header("run away to point within X bounds of the same room")]
		public Transform runAwayPoint;
		[Header("run away to center (root transform) of specified room node")]
		public RoomNode runAwayRoom;
        [Header("run away to item and do pick/swap action on it")]
        public InventoryItem runAwayItem;
        [Header("run away to interactive obj and start interaction, there's no check for items in this case")]
        public InteractionTarget runAwayInteraction;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            if (null != runAwayPoint)
            {
                Gizmos.DrawSphere(runAwayPoint.transform.position, .35f);
                Gizmos.DrawLine(transform.position, runAwayPoint.transform.position);
            }
            else if (null != runAwayRoom)
            {
                Gizmos.DrawSphere(runAwayRoom.transform.position, .35f);
                Gizmos.DrawLine(transform.position, runAwayRoom.transform.position);
            }
            else if (null != runAwayItem)
            {
                Gizmos.DrawSphere(runAwayItem.transform.position, .35f);
                Gizmos.DrawLine(transform.position, runAwayItem.transform.position);
            }
            else if (null != runAwayInteraction)
            {
                Gizmos.DrawSphere(runAwayInteraction.transform.position, .35f);
                Gizmos.DrawLine(transform.position, runAwayInteraction.transform.position);
            }
        }
    }
}
