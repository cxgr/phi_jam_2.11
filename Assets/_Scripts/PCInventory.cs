﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace Phijam
{
	public class PCInventory : MonoBehaviour
	{
		public InventoryItem heldItem;
		private Transform itemSlot;

        private GameObject ItemDisplay;
        private Image itemIcon;
		private Image secondaryItemIcon;
		
        void Awake()
        {
            itemSlot = new GameObject("itemSlot").transform;
            itemSlot.position = Vector3.up * 500f;

            var canvas = Instantiate(Resources.Load<GameObject>("InventoryCanvas"));
            ItemDisplay = canvas.transform.Find("ItemDisplay").gameObject;
            itemIcon = ItemDisplay.transform.Find("itemIcon").GetComponent<Image>();
			secondaryItemIcon = ItemDisplay.transform.Find("secondaryItemIcon").GetComponent<Image>();
		}

		public void HoldItem(InventoryItem item)
		{
			item.ToggleClickability(false);
			heldItem = item;
			heldItem.transform.parent = itemSlot;
			heldItem.transform.localPosition = Vector3.zero;
			heldItem.transform.localRotation = Quaternion.identity;

            var sr = heldItem.GetComponentInChildren<SpriteRenderer>();
            if (ItemDisplay && itemIcon && sr && sr.sprite)
            {
				ShowIcon(sr);
            }
		}

		public void LeaveItem(Transform tRef)
		{
			if (null == heldItem) return;
			heldItem.transform.parent = GetComponent<PCController>().currentRoom.transform;
			heldItem.transform.position = tRef.position;
			heldItem.transform.rotation = tRef.rotation;
			heldItem.ToggleClickability(true);
			heldItem = null;

            if (ItemDisplay && itemIcon)
            {
				HideIcon(itemIcon);
            }
		}

		public void DestroyItem()
		{
			if (null == heldItem) return;

			Destroy(heldItem.gameObject);
			heldItem = null;

			if (ItemDisplay && itemIcon)
			{
				HideIcon(itemIcon);
			}
		}

		public void ShowIcon(SpriteRenderer spriteSource)
		{
			ItemDisplay.SetActive(true);
			HideIcon(itemIcon);

			itemIcon.sprite = spriteSource.sprite;
			itemIcon.preserveAspect = true;

			itemIcon.color = new Color(1f, 1f, 1f, 0f);
			DG.Tweening.DOTweenModuleUI.DOFade(itemIcon, 1f, 1f);
		}

		private void HideIcon(Image icon)
		{
			var tween = DG.Tweening.DOTweenModuleUI.DOFade(icon, 0f, 1f);
			tween.onComplete += () =>
			{
				if (heldItem == null)
				{
					ItemDisplay.SetActive(false);
				}
				icon.sprite = null;
			};

			itemIcon = secondaryItemIcon;
			secondaryItemIcon = icon;
		}
	}
}
