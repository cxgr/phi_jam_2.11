﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "FX/Caustic"
{
	Properties
	{
		_Color("Main Color", Color) = (1, 1, 1, .5)
		_HighlightColor("Highlight Color", Color) = (1, 1, 1, .5)
		_Threshold("Threshold", Float) = 1
		_Caustic("Caustic", 2D) = "black" {}
		_CausticColor("Caustic Color", Color) = (1, 1, 1, .5)
		_CausticPeriod ("Caustic Period", Range (-5, 5)) = 0
		_Glow("Glow", 2D) = "white" {}
		_GlowingPeriod ("Glowing Period", Range (-5, 5)) = 0
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent+1" "RenderType" = "Transparent" }

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			ZTest LEqual
			Cull Off

			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform sampler2D _CameraDepthTexture; //Depth Texture
			uniform float4 _Color;
			uniform float4 _HighlightColor;
			uniform float _Threshold;
			uniform float _GlowingPeriod;
			uniform float _CausticPeriod;
			uniform sampler2D _Caustic;
			uniform float4 _Caustic_ST;
			uniform float4 _CausticColor;
			uniform sampler2D _Glow;
			uniform float4 _Glow_ST;

			struct appdata
			{
				float4 vertex : POSITION;
				float4 causticuv : TEXCOORD0;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 projPos : TEXCOORD1; //Screen position of pos
				float4 causticuv : TEXCOORD0; //Screen position of pos
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.projPos = ComputeScreenPos(o.pos);
				o.causticuv = v.causticuv;

				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				float2 timeOffset = float2(0, _Time.x);
				float4 glow = tex2D(_Glow, i.causticuv.xy * _Glow_ST.xy + _Glow_ST.zw + timeOffset * _GlowingPeriod);
				float4 texColor = _CausticColor * 2 
					* tex2D(_Caustic, i.causticuv.xy * _Caustic_ST.xy + _Caustic_ST.zw + timeOffset * _CausticPeriod + glow.xz);

				float4 finalColor = _Color;
				float blend = 1 - texColor.a;
				finalColor *= float4(blend, blend, blend, blend);
				return finalColor + texColor;
			}

			ENDCG
		}
		Pass
		{
			Blend One One
			ZWrite Off
			Cull Off

			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform sampler2D _CameraDepthTexture; //Depth Texture
			uniform float4 _Color;
			uniform float4 _HighlightColor;
			uniform float _Threshold;

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 projPos : TEXCOORD1; //Screen position of pos
			};

			v2f vert(appdata_base v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.projPos = ComputeScreenPos(o.pos);

				return o;
			}

			half4 frag(v2f i) : COLOR
			{
				float4 finalColor = float4(0, 0, 0, 0);

				//Get the distance to the camera from the depth buffer for this point
				float sceneZ = LinearEyeDepth(tex2Dproj(_CameraDepthTexture,
					UNITY_PROJ_COORD(i.projPos)).r);

				//Actual distance to the camera
				float partZ = i.projPos.z;

				//If the two are similar, then there is an object intersecting with our object
				float diff = (abs(sceneZ - partZ));
				diff *= diff;
				diff /= _Threshold * _Threshold;

				if (diff <= 1)
				{
					diff = 0;
					finalColor = lerp(_HighlightColor,
						float4(finalColor),
						float4(diff, diff, diff, diff));
				}

				half4 c;
				c.r = finalColor.r;
				c.g = finalColor.g;
				c.b = finalColor.b;
				c.a = finalColor.a;

				return c;
			}

			ENDCG
		}
	}
	FallBack "VertexLit"
}