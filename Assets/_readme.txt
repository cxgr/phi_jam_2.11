-- сцена:
_CHARACTER должен оперировать на z=0, внутренности комнат подстраиваются под это, рут комнат на z=0
коллайдер обстаклов подстраивается под z=0
почти все экшены самоподстраиваются под эту систему

-- ГГ (_CHARACTER)
коллайдер должен быть в слое 10:Player
важно назначить правильную CurrentRoom в PCController на старте
в PCMotor настраивается только WalkSpd, xzDir служебные
при скейле, смене WalkSpd и тд можно крутить скорость клипов в аниматоре
поля в PCInventory служебные

PCEmotes: синглтонится, вызывается откуда угодно, новый баббл перешибает старый
старые смайлы в _Prefabs/trash/emotes, диаметр 256px, ppu 100, если добавлять с теми же настройками, должно все совпасть

новые смайлы добавлять:
в PCEmotes классе enum EEmotes
в инспекторе в массив KeySpritePairs для каждого добавить структуру KeySpritePairs


-- комнаты
кнопка generate room работает только наполовину
для создания новой комнаты проще всего сдублировать старую и выпилить спрайты
глубину и высоту потолка скопировать с существующих 3
ширина пропорционально относительно game2.psb
roombox_split настраивается вручную, боковые стены выставлять на ширина в юнитах / 2
(или просто подождать меня)

у валидной комнаты должны быть:
clickable_zone коллайдер в слое 11:WalkableZone в районе z=0
RoomNode компонента
у присоединяемых к графу комнат в детях должны быть Portal
2 Portal надо сцепить через паблик поле (хотя бы одну из), дальше можно нажать _CONTROLLERS -> Pathfinding -> rebuild graph, связи разрулятся

*** если попадание в подвал будет через телепорт-интеракцию с лифтом, не портал в графе, не забыть в интеракции сетнуть current room у гг
подвал проще всего сделать отдельным подграфом из 1 комнаты

-- кликабельное
примеры см. в трансформе TEMPLATES, там комменты в инспекторе

важно:
у итемов 12:InventoryItems
у интерактивных зон слой 13:Interactives
у обстаклов 14:Obstacles

*** при добавлении нового кликабельного слоя не забыть добавить его в маску в _CONTROLLERS -> InputManager -> ClickableLayers

советы от кэпа:
кнопка от лифта - итем
подключение кнопки - интеракшен с required item == кнопка, в onFinished активируется лифт
лифт наверно тоже интеракшен, телепортирующий в подвал в onFinished
переключение миров - интеракшен
тентакли, шлющие прокрастинировать - обстакл с RunAwayInteraction в районе компа, прервать цепочку ходьбы будет нельзя

сильно мешать блендить 2 камеры служебные слои не должны

-- система экшенов
см PCActions за самими экшенами, PCController за примерами сборки цепочек
там ад и израиль, но в целом схема: InputManager стреляет эвент клика на чем-то отзывающемся на клик, PCController реагирует и пакует actionQ по ситуации

если придется дописывать действия:
1) abortQ()
2) если в очереди остались непрерываемые экшены, в конце действия НЕ нужно дописывать runQ(), т.к. старт функция текущего уже вызвана, у следующего вызовется автоматически
3) если текущее действие прерывалось (емнип сейчас это значит, что там был SimpleWalk), runQ нужно
4) вроде вообще все можно написать через WalkToHandler + новодописанный класс-наследник PCAction, проще всего копипастить существующие, чтобы не ковыряться в abort/run
5) экшены перехода через портал должны имплементировать IRoomTransferAction (кроме интеракции лифта, там проще тупо сетнуть комнату)


в самих экшенах плюем на все и управляем персонажем через SingletonUtils<ТипВажногоКонтроллера>.Instance.чето()

эффекты проще всего запилить централизованный контроллер-синглтон, и дергать его через onFinished интеракшенов или по мере надобности прямо из экшенов